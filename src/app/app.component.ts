import { Component } from '@angular/core';

import { ModalController, Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import "firebase/firestore";
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { ProfilUserPage } from './profil-user/profil-user.page';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {

  user: any;
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public firestore: AngularFirestore,
    public router: Router,
    public afAuth: AngularFireAuth,
    public modalController: ModalController

  ) {
    this.initializeApp();
  }

  ngOnInit(){
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

    async profil() {
      this.router.navigate(['profil-user'])
    }
 
    
  searching(){
    this.router.navigate(['search']);
  }
}
