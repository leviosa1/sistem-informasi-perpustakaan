import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController, ToastController } from '@ionic/angular';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';

interface User{
  displayName: string;
  email: string;
}

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

user: any = {};
userData: User;
  constructor(
    public afAuth: AngularFireAuth,
    public modalController:ModalController,
    public toastController: ToastController,
    public db: AngularFirestore,
    public router: Router,
  ) { }

  ngOnInit() {
  }

async pesanKesalahan(){
  const toast = await this.toastController.create({
    message: 'Registrasi Gagal! ',
    duration:  2000,
    position: 'middle'
  });
  toast.present();
}

register(){
  this.afAuth.createUserWithEmailAndPassword(this.user.email, this.user.password).then(res=>{
    this.writeuser(res.user.email);
  }, err=>{
    this.pesanKesalahan();
  })
}

writeuser(email)
{
  this.userData = {
    displayName: this.user.name,
    email: this.user.email,
  };
  this.db.collection('users').doc(email).set(this.userData).then(res=>{
    this.router.navigate(['/login']);
  }, err=>{
    this.pesanKesalahan();
  })
}

async login() {
  this.router.navigate(['/login'])
} 
}
