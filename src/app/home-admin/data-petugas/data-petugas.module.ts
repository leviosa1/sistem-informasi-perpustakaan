import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DataPetugasPageRoutingModule } from './data-petugas-routing.module';

import { DataPetugasPage } from './data-petugas.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DataPetugasPageRoutingModule
  ],
  declarations: [DataPetugasPage]
})
export class DataPetugasPageModule {}
