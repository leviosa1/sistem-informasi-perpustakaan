import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DataPetugasPage } from './data-petugas.page';

const routes: Routes = [
  {
    path: '',
    component: DataPetugasPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DataPetugasPageRoutingModule {}
