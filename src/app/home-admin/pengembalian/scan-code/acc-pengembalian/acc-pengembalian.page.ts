import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { PengembalianPage } from '../../pengembalian.page';

@Component({
  selector: 'app-acc-pengembalian',
  templateUrl: './acc-pengembalian.page.html',
  styleUrls: ['./acc-pengembalian.page.scss'],
})
export class AccPengembalianPage implements OnInit {

  constructor(
    public modalController:ModalController
  ) { }

  ngOnInit() {
  }

  async pengembalian() {
    const modal = await this.modalController.create({
      component: PengembalianPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }

  dismiss() {
    this.modalController.dismiss({
      'dismissed': true
    });
  }
}
