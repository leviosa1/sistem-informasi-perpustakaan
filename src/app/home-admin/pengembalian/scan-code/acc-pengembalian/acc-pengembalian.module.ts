import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AccPengembalianPageRoutingModule } from './acc-pengembalian-routing.module';

import { AccPengembalianPage } from './acc-pengembalian.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AccPengembalianPageRoutingModule
  ],
  declarations: [AccPengembalianPage]
})
export class AccPengembalianPageModule {}
