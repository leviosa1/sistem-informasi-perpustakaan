import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ScanCodePage } from './scan-code.page';

const routes: Routes = [
  {
    path: '',
    component: ScanCodePage
  },
  {
    path: 'acc-pengembalian',
    loadChildren: () => import('./acc-pengembalian/acc-pengembalian.module').then( m => m.AccPengembalianPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ScanCodePageRoutingModule {}
