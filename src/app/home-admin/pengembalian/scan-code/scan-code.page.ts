import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { PengembalianPage } from '../pengembalian.page';
import { AccPengembalianPage } from './acc-pengembalian/acc-pengembalian.page';

@Component({
  selector: 'app-scan-code',
  templateUrl: './scan-code.page.html',
  styleUrls: ['./scan-code.page.scss'],
})
export class ScanCodePage implements OnInit {

  constructor(
    public modalController:ModalController
  ) { }

  ngOnInit() {
  }

  async pengembalian() {
    const modal = await this.modalController.create({
      component: PengembalianPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }

  async savekembali() {
    const modal = await this.modalController.create({
      component: AccPengembalianPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
}
