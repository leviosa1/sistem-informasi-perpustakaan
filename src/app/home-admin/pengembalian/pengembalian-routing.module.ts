import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PengembalianPage } from './pengembalian.page';

const routes: Routes = [
  {
    path: '',
    component: PengembalianPage
  },
  {
    path: 'scan-code',
    loadChildren: () => import('./scan-code/scan-code.module').then( m => m.ScanCodePageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PengembalianPageRoutingModule {}
