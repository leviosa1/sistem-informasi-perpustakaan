import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditProfilUserPageRoutingModule } from './edit-profil-user-routing.module';

import { EditProfilUserPage } from './edit-profil-user.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EditProfilUserPageRoutingModule
  ],
  declarations: [EditProfilUserPage]
})
export class EditProfilUserPageModule {}
