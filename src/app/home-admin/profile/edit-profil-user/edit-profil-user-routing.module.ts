import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EditProfilUserPage } from './edit-profil-user.page';

const routes: Routes = [
  {
    path: '',
    component: EditProfilUserPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EditProfilUserPageRoutingModule {}
