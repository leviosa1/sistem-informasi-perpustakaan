import { Component, OnInit, Input } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { ToastService } from 'src/app/services/toast.service';
import { Plugins, CameraResultType } from '@capacitor/core';
import { ImageUploaderPage } from 'src/app/image-uploader/image-uploader.page';
const { Camera } = Plugins;
import {  Subject } from 'rxjs'
import { takeUntil} from 'rxjs/operators';


@Component({
  selector: 'app-edit-profil-user',
  templateUrl: './edit-profil-user.page.html',
  styleUrls: ['./edit-profil-user.page.scss'],
})
export class EditProfilUserPage implements OnInit {


  @Input() user: any;
  @Input() email: any;

  private ngUnsubscribe: Subject<void> = new Subject();
  constructor(
    private afs: AngularFirestore,
    private router: Router,
    public afAuth: AngularFireAuth,
    public toast: ToastService,
    public modalCtrl: ModalController
  ) { }

  userData:any={};
  ngOnInit()
  {
    this.afAuth.onAuthStateChanged(res=>{
      this.userData = res;
      this.getUser(res.email);
    })
  } 

  users:any = {};
  getUser(email)
  {
    this.afs.collection('users').doc(email).valueChanges()
    .pipe(takeUntil(this.ngUnsubscribe))
    .subscribe(res=>{
      this.users = res;
    })
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  
  saveData()
  {
    this.afs.collection('users').doc(this.email).update(this.user).then(res=>{
      this.dismiss();
    }).catch(err=>{
      this.toast.present('Tidak dapat menyimpan data, coba lagi.','top');
    })
  }

  updatingProfileImage: boolean;
  async takePicture() {
    const image = await Camera.getPhoto({
      quality: 90,
      allowEditing: true,
      resultType: CameraResultType.DataUrl
    });
    var imageUrl = image.dataUrl;
    var imagePath = this.userData.email+'/profile.png';
    const modal = await this.modalCtrl.create({
      component: ImageUploaderPage,
      cssClass: 'my-custom-class',
      componentProps:{imageData:imageUrl,imagePath: imagePath,ratio:1,width:100,height:100}
    }); 
    modal.onDidDismiss().then(res=>{
      if(res.data.imageUrl != false)
      {
        this.updateUserData({profileUrl:{url: res.data.imageUrl,ref:imagePath}});
        this.updateProfileUrl(res.data.imageUrl);
      }
    });
    return await modal.present(); 
  }

  updateProfileUrl(url)
  {    
    this.afAuth.onAuthStateChanged(res=>{
      res.updateProfile({photoURL:url});
    });
  }

  updateUserData(data)
  {
    this.updatingProfileImage = true;
    this.afs.collection('users').doc(this.userData.email).update(data).then(res=>{
      this.updatingProfileImage = false;
    }).catch(err=>{
      this.updatingProfileImage = false;
      this.toast.present('Tidak dapat mengganti foto profil, coba lagi.','top');
    })
  }

  dismiss()
  {
    this.modalCtrl.dismiss();
  }
}
