import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { ModalController } from '@ionic/angular';
import { HomeAdminPage } from '../home-admin.page';
import { Plugins, CameraResultType } from '@capacitor/core';
import { ImageUploaderPage } from 'src/app/image-uploader/image-uploader.page';
import { ToastService } from 'src/app/services/toast.service';
import { Router } from '@angular/router';
import { EditProfilUserPage } from './edit-profil-user/edit-profil-user.page';
const { Camera } = Plugins;
import {  Subject } from 'rxjs'
import { takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  private ngUnsubscribe: Subject<void> = new Subject();
  constructor(
    public modalController: ModalController,
    public afAuth: AngularFireAuth,
    public firestore: AngularFirestore,
    public toast: ToastService,
    public router: Router
  ) {
  
   }

   userData:any={};
  ngOnInit() {
    this.afAuth.onAuthStateChanged(res=>{
      this.userData = res;
      this.getUser(res.email);
    })
  }

  user:any = {};
  getUser(email)
  {
    this.firestore.collection('users').doc(email).valueChanges()
    .pipe(takeUntil(this.ngUnsubscribe))
    .subscribe(res=>{
      this.user = res;
    })
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }



  async dashboard() {
    const modal = await this.modalController.create({
      component: HomeAdminPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  } 

  async updateProfile()
  {
    const modal = await this.modalController.create({
      component: EditProfilUserPage,
      cssClass: 'my-custom-class',
      componentProps:{user: this.user, email: this.userData.email}
    });
    return await modal.present();
  }


}
