import { Component, OnInit } from '@angular/core';
import { ModalController, PopoverController } from '@ionic/angular';
import { HomeAdminPage } from '../home-admin.page';
import { PopoverComponent } from '../popover/popover.component';

@Component({
  selector: 'app-data-anggota',
  templateUrl: './data-anggota.page.html',
  styleUrls: ['./data-anggota.page.scss'],
})
export class DataAnggotaPage implements OnInit {

  constructor(
    public modalController:ModalController,
    public popCtrl:PopoverController
  ) { }

  ngOnInit() {
  }

  async dashboard() {
    const modal = await this.modalController.create({
      component: HomeAdminPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  } 


  async _popover(ev:any){
    //console.log("popover")
    const popover = await this.popCtrl.create({
      component:PopoverComponent,
      event : ev,
      cssClass: 'my-popover-class'
    })

    popover.onDidDismiss().then((data:any)=>
    console.log(data)
    )

    return await popover.present()
  }
}
