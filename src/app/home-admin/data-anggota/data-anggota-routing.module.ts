import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DataAnggotaPage } from './data-anggota.page';

const routes: Routes = [
  {
    path: '',
    component: DataAnggotaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DataAnggotaPageRoutingModule {}
