import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DataAnggotaPageRoutingModule } from './data-anggota-routing.module';

import { DataAnggotaPage } from './data-anggota.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DataAnggotaPageRoutingModule
  ],
  declarations: [DataAnggotaPage]
})
export class DataAnggotaPageModule {}
