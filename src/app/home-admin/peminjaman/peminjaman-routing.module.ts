import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PeminjamanPage } from './peminjaman.page';

const routes: Routes = [
  {
    path: '',
    component: PeminjamanPage
  },
  {
    path: 'acc-peminjaman',
    loadChildren: () => import('./acc-peminjaman/acc-peminjaman.module').then( m => m.AccPeminjamanPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PeminjamanPageRoutingModule {}
