import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { PeminjamanPage } from '../peminjaman.page';

@Component({
  selector: 'app-acc-peminjaman',
  templateUrl: './acc-peminjaman.page.html',
  styleUrls: ['./acc-peminjaman.page.scss'],
})
export class AccPeminjamanPage implements OnInit {

  scannedCode = null;
  constructor(
    public modalController: ModalController,
  ) { }

  ngOnInit() {
  }

  async peminjaman() {
    const modal = await this.modalController.create({
      component: PeminjamanPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }

  dismiss() {
    this.modalController.dismiss({
      'dismissed': true
    });
  }
}
