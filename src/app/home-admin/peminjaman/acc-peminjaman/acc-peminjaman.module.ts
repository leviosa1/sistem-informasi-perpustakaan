import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AccPeminjamanPageRoutingModule } from './acc-peminjaman-routing.module';

import { AccPeminjamanPage } from './acc-peminjaman.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AccPeminjamanPageRoutingModule
  ],
  declarations: [AccPeminjamanPage]
})
export class AccPeminjamanPageModule {}
