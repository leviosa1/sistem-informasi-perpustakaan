import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AccPeminjamanPage } from './acc-peminjaman.page';

const routes: Routes = [
  {
    path: '',
    component: AccPeminjamanPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AccPeminjamanPageRoutingModule {}
