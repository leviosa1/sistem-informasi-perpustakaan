import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DataBukuPage } from './data-buku.page';

const routes: Routes = [
  {
    path: '',
    component: DataBukuPage
  },
  {
    path: 'tambah-buku',
    loadChildren: () => import('./tambah-buku/tambah-buku.module').then( m => m.TambahBukuPageModule)
  },
  {
    path: 'detail-buku/:id',
    loadChildren: () => import('./detail-buku/detail-buku.module').then( m => m.DetailBukuPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DataBukuPageRoutingModule {}
