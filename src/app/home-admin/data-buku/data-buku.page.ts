import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController, ModalController, PopoverController } from '@ionic/angular';
import { FirestoreService } from 'src/app/services/firestore.service';
import { Book } from 'src/app/models/book';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-data-buku',
  templateUrl: './data-buku.page.html',
  styleUrls: ['./data-buku.page.scss'],
})
export class DataBukuPage implements OnInit {

  public bookList: Observable<Book[]>;
  imageURL: string;
  //public book: Observable<Book>;
  constructor(
    public modalController : ModalController,
    public popCtrl : PopoverController,
    public router: Router,
    private firestoreService : FirestoreService,
    private route: ActivatedRoute,
    private alertController: AlertController
  ) { }

  books: any[];
  ngOnInit() {
    //this.bookList = this.firestoreService.getBookList();
    this.getBooks();
  }

  getBooks(){
    this.firestoreService.getBook().subscribe((books) => {
      
      this.books = [];
      books.map(book => {
       this.books.push({
          id: book.payload.doc.id,
          data: book.payload.doc.data()
        })
      })
    })
  }

  deletePost(id:string, image: string){
    console.log(image)
    console.log(id)
    this.firestoreService.deletePost(id, image);
  }

  tambahbuku() {
    this.router.navigate(['/home-admin/data-buku/tambah-buku'])
  } 



}

