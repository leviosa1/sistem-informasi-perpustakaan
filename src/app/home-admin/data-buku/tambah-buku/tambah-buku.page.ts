import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { LoadingController, AlertController, ModalController } from '@ionic/angular';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { FirestoreService } from 'src/app/services/firestore.service';
import { AngularFireAuth } from '@angular/fire/auth';


@Component({
  selector: 'app-tambah-buku',
  templateUrl: './tambah-buku.page.html',
  styleUrls: ['./tambah-buku.page.scss'],
})
export class TambahBukuPage implements OnInit {

  /**public createBookForm: FormGroup;**/
  constructor(
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    private FirestoreService: FirestoreService,
    formBuilder: FormBuilder,
    private router: Router,
    public modalController: ModalController,
    public firestore: AngularFirestore,
    public afAuth: AngularFireAuth,
  ) { 
    /**this.createBookForm = formBuilder.group({
    judul: ['', Validators.required],
    kodeBuku: ['', Validators.required],
    penulis: ['', Validators.required],
    penerbit: ['', Validators.required],
    halaman: ['', Validators.required],
    stok: ['', Validators.required],
    deskripsi: ['', Validators.required],
    kategori: ['', Validators.required]
    });**/
  }
  
  ngOnInit() {
  }

  /**async createBook() {
    const loading = await this.loadingCtrl.create();
  
    const judul = this.createBookForm.value.judul;
    const kodeBuku = this.createBookForm.value.kodeBuku;
    const penulis = this.createBookForm.value.penulis;
    const penerbit = this.createBookForm.value.penerbit;
    const halaman = this.createBookForm.value.halaman;
    const stok = this.createBookForm.value.stok;
    const deskripsi = this.createBookForm.value.deskripsi;
    const kategori = this.createBookForm.value.kategori;
    /**const imageURL = this.createBookForm.value.imageURL;**/

    /**this.FirestoreService
      .createBook(judul, kodeBuku, penulis, penerbit, halaman, stok, deskripsi, kategori).then(() => {
          loading.dismiss().then(() => {
            this.router.navigate(['/home-admin/data-buku']);
          });
        },
        error => {
          loading.dismiss().then(() => {
            console.error(error);
          });
        }
      );

    return await loading.present();
  }**/
  image:any = null;
  public createBookForm = new FormGroup({
    judul: new FormControl('', Validators.required),
    kodeBuku: new FormControl('',  Validators.required),
    penulis: new FormControl('', Validators.required),
    penerbit: new FormControl('',  Validators.required),
    halaman: new FormControl('', Validators.required),
    stok: new FormControl('',  Validators.required),
    kategori: new FormControl('', Validators.required),
    deskripsi: new FormControl('',  Validators.required),
    cover: new FormControl('',  Validators.required),
  });

  public fileChanged($event: Event){
    //getting the image or files
    this.image = $event.target["files"];
    console.log(this.image);
  }

  public createBook(data: FormData){
    this.FirestoreService.createBook(data, this.image);
  }

}
