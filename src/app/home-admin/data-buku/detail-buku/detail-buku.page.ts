import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FirestoreService } from 'src/app/services/firestore.service';
import { Book } from 'src/app/models/book';

@Component({
  selector: 'app-detail-buku',
  templateUrl: './detail-buku.page.html',
  styleUrls: ['./detail-buku.page.scss'],
})
export class DetailBukuPage implements OnInit {

  public book: Book;
  constructor(
    public router: Router,
    private firestoreService : FirestoreService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    const bookId: string = this. route.snapshot.paramMap.get('id');
    this.firestoreService.getBookDetail(bookId).subscribe(books => {
      this.book = books;
    });
  }

}
