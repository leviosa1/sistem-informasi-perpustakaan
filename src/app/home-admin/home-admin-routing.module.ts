import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeAdminPage } from './home-admin.page';

const routes: Routes = [
  {
    path: '',
    component: HomeAdminPage
  },
  {
    path: 'peminjaman',
    loadChildren: () => import('./peminjaman/peminjaman.module').then( m => m.PeminjamanPageModule)
  },
  {
    path: 'data-buku',
    loadChildren: () => import('./data-buku/data-buku.module').then( m => m.DataBukuPageModule)
  },
  {
    path: 'data-anggota',
    loadChildren: () => import('./data-anggota/data-anggota.module').then( m => m.DataAnggotaPageModule)
  },
  {
    path: 'pengembalian',
    loadChildren: () => import('./pengembalian/pengembalian.module').then( m => m.PengembalianPageModule)
  },
  {
    path: 'profile',
    loadChildren: () => import('./profile/profile.module').then( m => m.ProfilePageModule)
  },
  {
    path: 'data-petugas',
    loadChildren: () => import('./data-petugas/data-petugas.module').then( m => m.DataPetugasPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeAdminPageRoutingModule {}
