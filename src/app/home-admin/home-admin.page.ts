import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { LoginPage } from '../login/login.page';
import { DataAnggotaPage } from './data-anggota/data-anggota.page';
import { DataBukuPage } from './data-buku/data-buku.page';
import { DataPetugasPage } from './data-petugas/data-petugas.page';
import { PeminjamanPage } from './peminjaman/peminjaman.page';
import { PengembalianPage } from './pengembalian/pengembalian.page';
import { ProfilePage } from './profile/profile.page';

@Component({
  selector: 'app-home-admin',
  templateUrl: './home-admin.page.html',
  styleUrls: ['./home-admin.page.scss'],
})
export class HomeAdminPage implements OnInit {

  constructor(
    public modalController : ModalController,
    public router: Router,
    public afAuth: AngularFireAuth
  ) { }

  ngOnInit() {
  }

  async klikpinjam() {
    const modal = await this.modalController.create({
      component: PeminjamanPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
  
  async klikdatabuku() {
    this.router.navigate(['/home-admin/data-buku'])

  }

  async klikdataanggota() {
    const modal = await this.modalController.create({
      component: DataAnggotaPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }

  async klikpengembalian() {
    const modal = await this.modalController.create({
      component: PengembalianPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }

  async logout() {
    this.afAuth.signOut().then(()=> {
      this.router.navigate(['/login']);
  })
  }

  async profil() {
    const modal = await this.modalController.create({
      component: ProfilePage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }

  async klikdatapetugas() {
    this.router.navigate(['/home-admin/data-petugas'])
  }
}
