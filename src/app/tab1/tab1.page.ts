import { Component } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { FirestoreService } from '../services/firestore.service';
import { AngularFirestore } from '@angular/fire/firestore';
import {  Subject } from 'rxjs';
import { takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  //filterTerm: string;
  
  private ngUnsubscribe: Subject<void> = new Subject();
  constructor(
    public modalController : ModalController,
    public router:Router,
    public afAuth: AngularFireAuth,
    private firebaseService: FirestoreService,
    public firestore: AngularFirestore

  ) {}

  books: any[];
  ngOnInit() {
    //this.bookList = this.firestoreService.getBookList();
    this.getBooks();
  }

  /**userRecords = [{
    "penulis" : 86,
    "title" : "Bahasa Indonesia"
  }]**/

  getBooks(){
    this.firebaseService.getBook()
    .pipe(takeUntil(this.ngUnsubscribe))
    .subscribe((books) => {
      
      this.books = [];
      books.map(book => {
       this.books.push({
          id: book.payload.doc.id,
          data: book.payload.doc.data()
        })
      })
     //console.log(this.books);
    })
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }


  doLogout(){
    this.afAuth.signOut().then(()=> {
      this.router.navigate(['/login']);
  })
  }

  

}

