import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Book } from '../models/book';
import { Observable } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';
import {finalize} from "rxjs/operators";
import { AngularFireStorage } from '@angular/fire/storage';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class FirestoreService {

  constructor(
    public firestore: AngularFirestore,
    public afAuth: AngularFireAuth,
    private storage: AngularFireStorage,
    public router: Router
  ) { 
  }

  downloadURL: Observable<string>;

  public getBook(){
    return this.firestore.collection("buku").snapshotChanges();
  }

  public deletePost(docID: string, image: string){
    
    //get the reference to the image
    const storageRef = this.storage.storage.ref();
    storageRef.child(image).delete()
    .then(()=>{
      console.log("image delete")
    }).catch(err => {
      console.log(err)
    });
    this.firestore.collection("buku").doc(docID)
    .delete().then(()=>{
     console.log("data buku deleted")     
    }).catch(err => {
      console.log(err)
    })
  }

  createBook(bookData: FormData, file: any[]){
    // console.log(file);
    const image = file[0];

    //loop through the files array of objects in case of multiple images

    const filepath = Date.now() + "-" + file[0]["name"];
    
    //get the reference
    const fileRef = this.storage.ref(filepath);
    const task = this.storage.upload(filepath, image)
    
    task.snapshotChanges().pipe(
      
      finalize(() => {
        console.log("File is being processed, you will be redirected soon...");
        fileRef.getDownloadURL().subscribe(url => {
          this.downloadURL = url;

          let newbook = {
            title : bookData["title"],
            id : bookData["id"],
            penulis : bookData["penulis"],
            penerbit : bookData["penerbit"],
            stok : bookData["stok"],
            category : bookData["category"],
            deskripsi : bookData["deskripsi"],
            halaman : bookData["halaman"],
            cover: this.downloadURL,
            fileref: filepath
          }

          //this.firestore.collection("posts").doc(filepath).set(newpost)

          this.firestore.collection("buku").add(newbook)
          .then(book => {
           /* get the reference of the document created
            console.log(post.path);
            this.firestore.doc(post.path).get().subscribe(x => {
              console.log(x.data());
            })
           */
            this.router.navigate(['/home-admin/data-buku'])
          }).catch(err => {
            console.log("error: ", err);
          });
          
          
        })

      })
    ).subscribe()
   }

   getBookDetail(bookId: string): Observable<Book>{
    return this.firestore.collection('buku').doc<Book>(bookId).valueChanges();
  } 

  /**createBook(
    judul: string,
    kodeBuku: string,
    penulis: string,
    penerbit: string,
    halaman: string,
    stok: string,
    deskripsi: string,
    kategori: string
  ): Promise<void>{
    const id = this.firestore.createId();

    return this.firestore.collection('buku').doc(id).set({
      id,
      judul,
      kodeBuku,
      penulis,
      penerbit,
      halaman,
      stok,
      deskripsi,
      kategori
    });
   }

   getBookDetail(bookId: string): Observable<Book>{
     return this.firestore.collection('buku').doc<Book>(bookId).valueChanges();
   }

   getBookList(): Observable<Book[]> {
    return this.firestore.collection<Book>('buku').valueChanges();
  }

  deleteBook(bookId: string): Promise<void> {
    return this.firestore.doc('buku/${bookId}').delete();
  }**/

}
