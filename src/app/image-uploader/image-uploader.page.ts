import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { AngularFireStorage } from '@angular/fire/storage';

@Component({
  selector: 'app-image-uploader',
  templateUrl: './image-uploader.page.html',
  styleUrls: ['./image-uploader.page.scss'],
})
export class ImageUploaderPage implements OnInit {
  @Input() imageData: any;
  @Input() imagePath: any;
  @Input() ratio: any;
  @Input() width: any;
  @Input() height: any;
  maintainRatio: boolean = false;
  constructor(
    public modalCtrl: ModalController,
    public storage: AngularFireStorage
  ) { }

  ngOnInit() {
    if(this.ratio == undefined){
      this.ratio = 0;
    }else{
      this.maintainRatio = true;
    }
    if(this.width == undefined) this.width = 0
    if(this.height = undefined) this.height = 0;
  }

  //cropper
  croppedImage: any = '';    
  imageCropped(event: ImageCroppedEvent) {
      this.croppedImage = event.base64;
  } 
  //end cropper

  loading: boolean;
  upload()
  {
    if(this.imagePath != false)
    {
      this.loading = true;
      var ref = this.storage.ref(this.imagePath);   
      ref.putString(this.croppedImage,'data_url').then(res=>{
        this.getUrl();
      }).catch(err=>{
        this.loading = false;
        this.dismiss();
      });
    }else{
      this.modalCtrl.dismiss({
        'imageUrl': this.croppedImage
      });
    }
     
  }

  getUrl()
  {
    this.storage.ref(this.imagePath).getDownloadURL().subscribe(url=>{
      this.loading = false;
      this.modalCtrl.dismiss({
        'imageUrl': url
      });
    },err=>{
      this.loading = false;
    })
  }

  dismiss() {    
    this.modalCtrl.dismiss({
      'imageUrl': false
    });
  }

}
