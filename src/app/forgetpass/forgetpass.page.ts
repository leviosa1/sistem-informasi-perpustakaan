import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { AlertController, ToastController } from '@ionic/angular';
import { ToastService } from '../services/toast.service';


@Component({
  selector: 'app-forgetpass',
  templateUrl: './forgetpass.page.html',
  styleUrls: ['./forgetpass.page.scss'],
})
export class ForgetpassPage implements OnInit {

  constructor(
    private alertCtrl : AlertController,
    public router:Router,
    public afAuth:AngularFireAuth,
    public toastController: ToastController,
    public toast: ToastService
  ) { }
  ngOnInit() {
  }

  userAuth:any;
  ionViewDidEnter() {
    this.afAuth.onAuthStateChanged((user) => {
      if (user) {
        this.userAuth = user;
      }
    })
  }

  async pesanKesalahan() {
    const toast = await this.toastController.create({
      message: 'Your settings have been saved.',
      duration: 2000,
      position:'middle'
    });
    toast.present();
  }

  email:any;
  loading:boolean;
  forgot()
  {
    this.loading=true;
    this.afAuth.sendPasswordResetEmail(this.email).then(res=>{
      this.loading=false;
      this.router.navigate(['/login']);
    }).catch(error=>{
      this.pesanKesalahan();
    })
  }

  async login() {
    this.router.navigate(['/login'])
  } 
}
