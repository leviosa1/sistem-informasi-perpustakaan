export interface Book {
    id : string;
    title: string;
    penulis: string;
    penerbit: string;
    halaman: string;
    stok: string;
    deskripsi : string;
    category: string;
    cover: string;
}



