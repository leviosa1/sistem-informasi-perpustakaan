export interface User {
    name : string;
    email: string;
    noAnggota: string;
    address: string;
}
