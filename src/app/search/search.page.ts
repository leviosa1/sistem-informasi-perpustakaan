import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { FirestoreService } from '../services/firestore.service';
import {  Subject } from 'rxjs';
import { takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-search',
  templateUrl: './search.page.html',
  styleUrls: ['./search.page.scss'],
})
export class SearchPage implements OnInit {

  sampleArr = [];
  resultArr=[];
  private ngUnsubscribe: Subject<void> = new Subject();
  constructor(
    public firestore: AngularFirestore,
    public firebaseService: FirestoreService
  ) { }

  books: any[];
  ngOnInit() {
    this.getBooks();
  }

  getBooks(){
    this.firebaseService.getBook()
    .pipe(takeUntil(this.ngUnsubscribe))
    .subscribe((books) => {
      
      this.books = [];
      books.map(book => {
       this.books.push({
          id: book.payload.doc.id,
          data: book.payload.doc.data()
        })
      })
     //console.log(this.books);
    })
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  search(event){
    let searchKey:string=event.target.value;
    let firstLetter=searchKey.toUpperCase();

    if(searchKey.length == 0){
      this.sampleArr=[];
      this.resultArr=[];
    }

    if(this.sampleArr.length==0){
      this.firestore.collection('buku',ref=>ref.where('SearchIndex','==',firstLetter)).snapshotChanges()
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(data=>{
        data.forEach(childData=>{
          this.sampleArr.push(childData.payload.doc.data())
        })
      })
    }
    else{
      this.resultArr=[];
      this.sampleArr.forEach(val=>{
        let title:string=val['title'];
        if(title.toUpperCase().startsWith(searchKey.toUpperCase())){
          if(true){
            this.resultArr.push(val);
          }
        }
      })
    }
  }
}
