import { Component } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { KonfirmasiPemesananPage } from '../detail-buku/konfirmasi-pemesanan/konfirmasi-pemesanan.page';
import { KonfirmasiPengembalianPage } from '../detail-buku/konfirmasi-pengembalian/konfirmasi-pengembalian.page';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  
  cart: Array<any> = [];

  public selectedSegment:string='pinjam'
  constructor(
    public modalController : ModalController
  ) {}

  segmentChanged(event:any){
    console.log(event.target.value);
    this.selectedSegment=event.target.value;

    if(localStorage.getItem("carts")){
      this.cart = JSON.parse(localStorage.getItem("carts"));
    }

    
  }

  async konfpemesanan() {
    const modal = await this.modalController.create({
      component: KonfirmasiPemesananPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }

  async konfkembali() {
    const modal = await this.modalController.create({
      component: KonfirmasiPengembalianPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }

}
