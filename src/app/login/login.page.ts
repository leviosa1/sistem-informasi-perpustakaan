import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { AngularFireAuth } from '@angular/fire/auth';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AngularFirestore } from '@angular/fire/firestore';
import {  Subject } from 'rxjs';
import { takeUntil} from 'rxjs/operators';

interface User {
  id: string;
  displayName: string;
  address: string;
  image: String;
}


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  /**email : string= ""
  password : string= ""**/
  public loginForm: FormGroup;
  user: any = {};
  userData: User;
  private ngUnsubscribe: Subject<void> = new Subject();
  constructor(
    public router:Router,
    public afAuth: AngularFireAuth,
    formBuilder: FormBuilder,
    public db: AngularFirestore
  ) { 
    this.loginForm = formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
      });
  }

  ngOnInit(
    
  ) {
  }
  
  
  async login(){
    try {
        const res = await this.afAuth.signInWithEmailAndPassword(this.loginForm.value['email'], this.loginForm.value['password'])
        .then(res => {
          if(res.user){
            this.cekRole(res.user.email);
          }
        })
    } catch(err) {
      console.dir(err)
      if(err.code === "auth/user-not-found"){
        console.log("User Not Found")
      }
    }
  }

  async cekRole(email) {
    this.db.collection('users').doc(email).get()
    .pipe(takeUntil(this.ngUnsubscribe))
    .subscribe(res => {
      this.routerControl(res.data());
    })

  }

  
  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  routerControl(data) {
    if (data.role == 'admin') this.router.navigate(['/home-admin']);
    if (data.role == 'user') this.router.navigate(['/tabs']);
  }


  goToReset(){
    this.router.navigateByUrl('/forgetpass');
  }

  async goToSignUp() {
    this.router.navigate(['/register'])
  }  
}
