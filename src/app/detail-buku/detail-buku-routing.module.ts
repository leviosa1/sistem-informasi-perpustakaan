import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetailBukuPage } from './detail-buku.page';

const routes: Routes = [
  {
    path: '',
    component: DetailBukuPage
  },
  {
    path: 'konfirmasi-pemesanan',
    loadChildren: () => import('./konfirmasi-pemesanan/konfirmasi-pemesanan.module').then( m => m.KonfirmasiPemesananPageModule)
  },
  {
    path: 'konfirmasi-pengembalian',
    loadChildren: () => import('./konfirmasi-pengembalian/konfirmasi-pengembalian.module').then( m => m.KonfirmasiPengembalianPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetailBukuPageRoutingModule {}
