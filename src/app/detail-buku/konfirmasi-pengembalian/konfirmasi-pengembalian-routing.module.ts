import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { KonfirmasiPengembalianPage } from './konfirmasi-pengembalian.page';

const routes: Routes = [
  {
    path: '',
    component: KonfirmasiPengembalianPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class KonfirmasiPengembalianPageRoutingModule {}
