import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { KonfirmasiPengembalianPage } from './konfirmasi-pengembalian.page';

describe('KonfirmasiPengembalianPage', () => {
  let component: KonfirmasiPengembalianPage;
  let fixture: ComponentFixture<KonfirmasiPengembalianPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ KonfirmasiPengembalianPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(KonfirmasiPengembalianPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
