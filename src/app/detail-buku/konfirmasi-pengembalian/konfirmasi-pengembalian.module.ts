import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { KonfirmasiPengembalianPageRoutingModule } from './konfirmasi-pengembalian-routing.module';

import { KonfirmasiPengembalianPage } from './konfirmasi-pengembalian.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    KonfirmasiPengembalianPageRoutingModule
  ],
  declarations: [KonfirmasiPengembalianPage]
})
export class KonfirmasiPengembalianPageModule {}
