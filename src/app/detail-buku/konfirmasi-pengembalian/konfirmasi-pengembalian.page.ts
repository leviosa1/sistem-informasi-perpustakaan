import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-konfirmasi-pengembalian',
  templateUrl: './konfirmasi-pengembalian.page.html',
  styleUrls: ['./konfirmasi-pengembalian.page.scss'],
})
export class KonfirmasiPengembalianPage implements OnInit {

  constructor(
    public modalCtrl : ModalController
  ) { }

  ngOnInit() {
  }

  dismiss() {
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }
}
