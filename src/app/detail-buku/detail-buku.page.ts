import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { Book } from '../models/book';
import { FirestoreService } from '../services/firestore.service';

@Component({
  selector: 'app-detail-buku',
  templateUrl: './detail-buku.page.html',
  styleUrls: ['./detail-buku.page.scss'],
})
export class DetailBukuPage implements OnInit {

  public book: Book;
  addText:string;
  bookId:string;
  title: string;
  constructor(
    public modalController: ModalController,
    public router:Router,
    private firestoreService : FirestoreService,
    private route: ActivatedRoute

  ) { }

  ngOnInit() {
    const bookId: string = this. route.snapshot.paramMap.get('id');
    this.firestoreService.getBookDetail(bookId).subscribe(books => {
      this.book = books;
    });

    if(localStorage.getItem("carts")){
      let carts: Array<any> = JSON.parse(localStorage.getItem("carts"));

      let index = carts.findIndex(x => x.id == this.bookId);
      if (index == -1){
        this.addText = "Pesan";
      }
      else{
        this.addText = "Terpesan";
      }
    }
    else{
      this.addText = "Pesan"
    }
  }

  addtoPesan(){
    let carts: Array<any> = [];
    if(localStorage.getItem("carts")){
      carts = JSON.parse(localStorage.getItem("carts"));

      let index = carts.findIndex(x => x.id == this.bookId);

      if(index == -1){
        this.addText = "Terpesan";
        var obj = {
          id: this.bookId,
          title: this.title,
        }

        carts.push(obj);
      }
      else{
        carts.splice(index, 1);
        this.addText = "Pesan";
      }
    }
    else{
      this.addText= "Pesan";
      var obj = {
        id: this.bookId,
        title: this.title,
      }
      carts.push(obj);
    }

    localStorage.setItem("carts", JSON.stringify(carts));
  }
}