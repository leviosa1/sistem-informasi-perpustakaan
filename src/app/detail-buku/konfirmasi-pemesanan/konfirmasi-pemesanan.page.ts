import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, ModalController } from '@ionic/angular';
import { Tab1Page } from 'src/app/tab1/tab1.page';
import { Tab2Page } from 'src/app/tab2/tab2.page';
import { Konfpemesanan2Page } from './konfpemesanan2/konfpemesanan2.page';

@Component({
  selector: 'app-konfirmasi-pemesanan',
  templateUrl: './konfirmasi-pemesanan.page.html',
  styleUrls: ['./konfirmasi-pemesanan.page.scss'],
})
export class KonfirmasiPemesananPage implements OnInit {

  constructor(
    public modalController : ModalController,
    public alertController: AlertController,
    public router:Router
  ) { }

  ngOnInit() {
  }
 
  dissmiss() {
    this.modalController.dismiss({
      'dismissed': true
    });
  }

  async konfpesan() {
    const modal = await this.modalController.create({
      component: Konfpemesanan2Page,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }

  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      message: '<strong>Anda Yakin Ingin Membatalkan Pemesanan Buku?</strong>',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Okay',
          handler: () => {
            console.log('Confirm Okay');
          }
        }
      ]
    });

    await alert.present();
  }
}
