import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { KonfirmasiPemesananPage } from './konfirmasi-pemesanan.page';

const routes: Routes = [
  {
    path: '',
    component: KonfirmasiPemesananPage
  },
  {
    path: 'konfpemesanan2',
    loadChildren: () => import('./konfpemesanan2/konfpemesanan2.module').then( m => m.Konfpemesanan2PageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class KonfirmasiPemesananPageRoutingModule {}
