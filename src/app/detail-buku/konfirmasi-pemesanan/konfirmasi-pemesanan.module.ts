import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { KonfirmasiPemesananPageRoutingModule } from './konfirmasi-pemesanan-routing.module';

import { KonfirmasiPemesananPage } from './konfirmasi-pemesanan.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    KonfirmasiPemesananPageRoutingModule
  ],
  declarations: [KonfirmasiPemesananPage]
})
export class KonfirmasiPemesananPageModule {}
