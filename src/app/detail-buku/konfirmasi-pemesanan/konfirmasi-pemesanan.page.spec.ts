import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { KonfirmasiPemesananPage } from './konfirmasi-pemesanan.page';

describe('KonfirmasiPemesananPage', () => {
  let component: KonfirmasiPemesananPage;
  let fixture: ComponentFixture<KonfirmasiPemesananPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ KonfirmasiPemesananPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(KonfirmasiPemesananPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
