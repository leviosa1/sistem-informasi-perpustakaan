import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Konfpemesanan2Page } from './konfpemesanan2.page';

const routes: Routes = [
  {
    path: '',
    component: Konfpemesanan2Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Konfpemesanan2PageRoutingModule {}
