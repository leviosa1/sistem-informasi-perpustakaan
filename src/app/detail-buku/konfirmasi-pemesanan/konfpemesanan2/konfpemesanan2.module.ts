import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Konfpemesanan2PageRoutingModule } from './konfpemesanan2-routing.module';

import { Konfpemesanan2Page } from './konfpemesanan2.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Konfpemesanan2PageRoutingModule
  ],
  declarations: [Konfpemesanan2Page]
})
export class Konfpemesanan2PageModule {}
