import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-konfpemesanan2',
  templateUrl: './konfpemesanan2.page.html',
  styleUrls: ['./konfpemesanan2.page.scss'],
})
export class Konfpemesanan2Page implements OnInit {

  constructor(
    public modalCtrl : ModalController
  ) { }

  ngOnInit() {
  }

  dismiss() {
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }
}
