import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Konfpemesanan2Page } from './konfpemesanan2.page';

describe('Konfpemesanan2Page', () => {
  let component: Konfpemesanan2Page;
  let fixture: ComponentFixture<Konfpemesanan2Page>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ Konfpemesanan2Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Konfpemesanan2Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
