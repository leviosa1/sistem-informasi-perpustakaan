import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProfilUserPage } from './profil-user.page';

const routes: Routes = [
  {
    path: '',
    component: ProfilUserPage
  },
  {
    path: 'edit-profil-user',
    loadChildren: () => import('./edit-profil-user/edit-profil-user.module').then( m => m.EditProfilUserPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfilUserPageRoutingModule {}
