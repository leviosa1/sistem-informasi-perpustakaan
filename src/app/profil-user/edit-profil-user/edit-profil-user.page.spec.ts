import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EditProfilUserPage } from './edit-profil-user.page';

describe('EditProfilUserPage', () => {
  let component: EditProfilUserPage;
  let fixture: ComponentFixture<EditProfilUserPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditProfilUserPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EditProfilUserPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
