import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { ToastService } from '../services/toast.service';
import { TabsPage } from '../tabs/tabs.page';
import { EditProfilUserPage } from './edit-profil-user/edit-profil-user.page';
import {  Subject } from 'rxjs';
import { takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-profil-user',
  templateUrl: './profil-user.page.html',
  styleUrls: ['./profil-user.page.scss'],
})
export class ProfilUserPage implements OnInit {

  private ngUnsubscribe: Subject<void> = new Subject();
  constructor(
    public modalController: ModalController,
    public afAuth: AngularFireAuth,
    public firestore: AngularFirestore,
    public toast: ToastService,
    public router: Router
  ) {
  
   }

   userData:any={};
  ngOnInit() {
    this.afAuth.onAuthStateChanged(res=>{
      this.userData = res;
      this.getUser(res.email);
    })
  }

  user:any = {};
  getUser(email)
  {
    this.firestore.collection('users').doc(email).valueChanges()
    .pipe(takeUntil(this.ngUnsubscribe))
    .subscribe(res=>{
      this.user = res;
    })
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }


    async updateProfile()
    {
      const modal = await this.modalController.create({
        component: EditProfilUserPage,
        cssClass: 'my-custom-class',
        componentProps:{user: this.user, email: this.userData.email}
      });
      return await modal.present();
    }
  

  dashboard() {
    this.router.navigate(['/tabs/tab1'])
  } 
}
