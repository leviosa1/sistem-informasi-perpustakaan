import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ProfilUserPage } from './profil-user.page';

describe('ProfilUserPage', () => {
  let component: ProfilUserPage;
  let fixture: ComponentFixture<ProfilUserPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfilUserPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ProfilUserPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
